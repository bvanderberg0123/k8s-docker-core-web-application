using K8SDockerCoreWebApi.Contexts;
using Microsoft.AspNetCore.Mvc;

namespace K8SDockerCoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        [HttpGet]
        public JsonResult Get()
        {
            K8SDemoContext context = HttpContext.RequestServices.GetService(typeof(K8SDemoContext)) as K8SDemoContext;
            return new JsonResult(context.GetAllFilms());
        }
    }
}