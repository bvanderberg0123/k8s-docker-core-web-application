**minikube start --extra-config=apiserver.service-node-port-range=80-30000**
**eval $(minikube docker-env)**
**echo $(minikube service k8s-demo --url)**

**GRANT ALL PRIVILEGES ON k8s_demo.* TO 'root'@'%';**
**GRANT ALL PRIVILEGES ON k8s_demo.* TO 'root'@'localhost';**
**FLUSH PRIVILEGES;**

**kubectl set image deployment k8s-demo k8s-demo=k8s-demo:1.0.3**
**kubectl rollout history deployment k8s-demo**
**kubectl rollout undo deployment k8s-demgito --to-revision 1**