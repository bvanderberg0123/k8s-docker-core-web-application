using K8SDockerCoreWebApplication.Contexts;
using Microsoft.AspNetCore.Mvc;

namespace K8SDockerCoreWebApplication.Controllers
{
    public class FilmsController : Controller
    {
        // GET
        public IActionResult Index()
        {
            K8SDemoContext context = HttpContext.RequestServices.GetService(typeof(K8SDemoContext)) as K8SDemoContext;
            
            return View(context.GetAllFilms());
        }
    }
}