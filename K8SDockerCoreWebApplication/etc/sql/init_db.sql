CREATE DATABASE k8s_demo;

USE k8s_demo;

CREATE TABLE film
( film_id int NOT NULL AUTO_INCREMENT,
  title varchar(50) NOT NULL,
  description varchar(255),
  release_year int NOT NULL,
  length int NOT NULL,
  rating VARCHAR(50) NOT NULL,
  CONSTRAINT film_pk PRIMARY KEY (film_id)
);

insert into film (title,description,release_year,length,rating) values("Avatar","The Blue people",2013,120,"Good");

GRANT ALL PRIVILEGES ON k8s_demo.* TO 'root'@'%';
GRANT ALL PRIVILEGES ON k8s_demo.* TO 'root'@'localhost';
FLUSH PRIVILEGES;